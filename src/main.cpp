/*
 * Sample program for DokodeModem
 * E2PROM sample
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();

void setup()
{
  Dm.begin(); // 初期化が必要です。

  uint8_t e2data;

  Dm.e2prom_read(0, &e2data);// 読み出し
  e2data += 1;
  Dm.e2prom_write(0, e2data);// 書き込み
}

void loop()
{
}
